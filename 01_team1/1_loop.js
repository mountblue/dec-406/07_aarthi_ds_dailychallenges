// User defined class node 
class Node { 
    // constructor 
    constructor(element) 
    { 
        this.element = element; 
        this.next = null
    } 
} 
// linkedlist class 
class LinkedList { 
    constructor() 
    { 
        this.head = null; 
        this.size = 0; 
    }
    // adds an element at the end 
	// of list 
	add(element) 
	{ 
	    // creates a new node 
	    var node = new Node(element); 
	  
	    // to store current node 
	    var current; 
	  
	    // if list is Empty add the 
	    // element and make it head 
	    if (this.head == null) 
	        this.head = node; 
	    else { 
	        current = this.head; 
	  
	        // iterate to the end of the 
	        // list 
	        while (current.next) { 
	            current = current.next; 
	        } 
	  
	        // add node 
	        current.next = node; 
	    } 
	    this.size++; 
	} 
	insertAt(element, index) 
	{ 
	    if (index > 0 && index > this.size) 
	        return false; 
	    else { 
	        // creates a new node 
	        var node = new Node(element); 
	        var curr, prev; 
	  
	        curr = this.head; 
	  
	        // add the element to the 
	        // first index 
	        if (index == 0) { 
	            node.next = head; 
	            this.head = node; 
	        } else { 
	            curr = this.head; 
	            var it = 0; 
	  
	            // iterate over the list to find 
	            // the position to insert 
	            while (it < index) { 
	                it++; 
	                prev = curr; 
	                curr = curr.next; 
	            } 
	  
	            // adding an element 
	            node.next = curr; 
	            prev.next = node; 
	        } 
	        this.size++; 
	    } 
	} 
	removeFrom(index) 
	{ 
	    if (index > 0 && index > this.size) 
	        return -1; 
	    else { 
	        var curr, prev, it = 0; 
	        curr = this.head; 
	        prev = curr; 
	  
	        // deleting first element 
	        if (index == 0) { 
	            this.head = curr.next; 
	        } else { 
	            // iterate over the list to the 
	            // position to removce an element 
	            while (it < index) { 
	                it++; 
	                prev = curr; 
	                curr = curr.next; 
	            } 
	  
	            // remove the element 
	            prev.next = curr.next; 
	        } 
	        this.size--; 
	  
	        // return the remove element 
	        return curr.element; 
	    } 
	} 
	removeElement(element) 
	{ 
	    var current = this.head; 
	    var prev = null; 
	  
	    // iterate over the list 
	    while (current != null) { 
	        // comparing element with current 
	        // element if found then remove the 
	        // and return true 
	        if (current.element == element) { 
	            if (prev == null) { 
	                this.head = current.next; 
	            } else { 
	                prev.next = current.next; 
	            } 
	            this.size--; 
	            return current.element; 
	        } 
	        prev = current; 
	        current = current.next; 
	    } 
	    return -1; 
	}
	printList() 
	{ 
	    var curr = this.head; 
	    var str = ""; 
	    while (curr) { 
	        str += curr.element + " "; 
	        curr = curr.next; 
	    } 
	    console.log(str); 
	}
	Solution(){
		var curr1= this.head;
		var curr2= this.head;
		while(curr2){
			curr2=curr2.next.next;
			if(curr1.element==curr2.element){
				console.log("loop exists");
				curr1=curr1.next;
				while(curr2.element!=curr1.element){
					count++;
					curr1=curr1.next;
				}
				console.log(count);
				break;
			}
			curr1=curr1.next;
		}
	}
}
var count=0;
var N=7;
var LL= new LinkedList();
LL.head=new Node(17);
LL.head.next=new Node(15);
LL.head.next.next=new Node(8);
LL.head.next.next.next=new Node(9);
LL.head.next.next.next.next=LL.head.next;
LL.Solution();
